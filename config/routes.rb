Rails.application.routes.draw do
  resources :participants
  resources :slots
  resources :events
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'events#index'
  get 'standard', to: 'theme#standard'
  get 'listed', to: 'theme#listed'
  get 'boxed', to: 'theme#boxed'

  get 'events(/:id)/participants' => 'participants#event_participants', as: :event_participants
end
