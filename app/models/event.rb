# == Schema Information
#
# Table name: events
#
#  id             :integer          not null, primary key
#  description    :text
#  end_date       :date
#  end_time       :time
#  location       :string
#  number_of_days :integer
#  room           :string
#  start_date     :date
#  start_time     :time
#  title          :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Event < ApplicationRecord
	has_many  :slots,
            dependent: :destroy,
            foreign_key: 'event_id',
            primary_key: 'id'

  has_many  :participants,
            dependent: :destroy,
            foreign_key: 'event_id',
            primary_key: 'id'

	def number_of_days
		(end_date - start_date).to_i
	end

  def days
    (start_date..end_date).to_a
  end	
end
