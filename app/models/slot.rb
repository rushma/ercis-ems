# == Schema Information
#
# Table name: slots
#
#  id         :integer          not null, primary key
#  date       :date
#  end_time   :time
#  location   :string
#  room       :string
#  speaker    :string
#  start_time :time
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  event_id   :integer
#

class Slot < ApplicationRecord
	belongs_to :event, foreign_key: 'event_id', primary_key: 'id'
	validates :event_id, presence: true

  def duration
    ((end_time - start_time).to_i) / 60 #converted to minutes
  end
end
