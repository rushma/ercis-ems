# == Schema Information
#
# Table name: participants
#
#  id                :integer          not null, primary key
#  affiliation       :string
#  email             :string
#  first_name        :string
#  is_contact_person :boolean
#  last_name         :string
#  telephone         :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  event_id          :integer
#

class Participant < ApplicationRecord
  belongs_to :event, foreign_key: 'event_id', primary_key: 'id'
  validates :event_id, presence: true
end
