json.extract! participant, :id, :first_name, :last_name, :affiliation, :email, :telephone, :is_contact_person, :created_at, :updated_at
json.url participant_url(participant, format: :json)
