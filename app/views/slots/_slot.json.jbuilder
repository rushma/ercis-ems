json.extract! slot, :id, :id, :start_date, :end_date, :duration, :title, :room, :location, :created_at, :updated_at
json.url slot_url(slot, format: :json)
