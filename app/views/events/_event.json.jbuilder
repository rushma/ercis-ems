json.extract! event, :id, :id, :title, :description, :start_date, :end_date, :location, :created_at, :updated_at
json.url event_url(event, format: :json)
