class RemoveStartAndEndDateAndAddOnlyDateFieldToSlots < ActiveRecord::Migration[5.2]
  def change
    remove_column :slots, :start_date
    remove_column :slots, :end_date
    add_column :slots, :date, :date
  end
end
