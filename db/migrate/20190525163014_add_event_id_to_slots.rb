class AddEventIdToSlots < ActiveRecord::Migration[5.2]
  def change
  	add_column :slots, :event_id, :integer
  end
end
