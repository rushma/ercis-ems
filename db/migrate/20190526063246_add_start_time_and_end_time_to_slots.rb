class AddStartTimeAndEndTimeToSlots < ActiveRecord::Migration[5.2]
  def change
  	add_column :slots, :start_time, :time
  	add_column :slots, :end_time, :time
  end
end
