class CreateSlots < ActiveRecord::Migration[5.2]
  def change
    create_table :slots, id: false do |t|
      t.string :id
      t.datetime :start_date
      t.datetime :end_date
      t.integer :duration
      t.string :title
      t.string :room
      t.string :location

      t.timestamps
    end
  end
end
