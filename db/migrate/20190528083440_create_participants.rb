class CreateParticipants < ActiveRecord::Migration[5.2]
  def change
    create_table :participants do |t|
      t.string :first_name
      t.string :last_name
      t.string :affiliation
      t.string :email
      t.string :telephone
      t.boolean :is_contact_person

      t.timestamps
    end
  end
end
