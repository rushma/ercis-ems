class AddRoomToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :room, :string
  end
end
