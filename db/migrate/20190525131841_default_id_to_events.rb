class DefaultIdToEvents < ActiveRecord::Migration[5.2]
  def change
  	remove_column :events, :id
  	add_column :events, :id, :primary_key
  end
end
