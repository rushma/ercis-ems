class AddNumberOfDaysToEvents < ActiveRecord::Migration[5.2]
  def change
  	add_column :events, :number_of_days, :integer
  end
end
