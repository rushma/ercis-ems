class AddSpeakerToSlot < ActiveRecord::Migration[5.2]
  def change
    add_column :slots, :speaker, :string
  end
end
