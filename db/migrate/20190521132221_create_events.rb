class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events, id:false do |t|
      t.integer :id, primary_key: true
      t.string :title
      t.text :description
      t.datetime :start_date
      t.datetime :end_date
      t.string :location

      t.timestamps
    end
  end
end
