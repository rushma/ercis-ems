class DefaultIdToSlots < ActiveRecord::Migration[5.2]
  def change
  	remove_column :slots, :id
  	add_column :slots, :id, :primary_key
  end
end
