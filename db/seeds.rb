# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if Rails.env == 'development'
  Event.delete_all
  Slot.delete_all
end

15.times do |i|
	event = Event.create!(
    id: i,
    location: ['Leonardo-Campus 18, 48149 Münster', 'Leonardo-Campus 3, 48149 Münster', 'Leonardo-Campus 5, 48149 Münster'].sample,
    start_date: Date.today,
    end_date: Date.today + 2.day,
    start_time: [Time.now, Time.now - 1.hour, Time.now + 1.hour].sample,
    end_time: [Time.now + 8.hour, Time.now + 12.hour, Time.now + 100.hour].sample,
    title: "Event-#{i}",
    description: "This is test database.",
    room: ['114', '022', '201'].sample
  )

  5.times do |j|
    Slot.create!(
      id: 25..30,
      event_id: event.id,
      date: event.start_date,
      start_time: [Time.now, Time.now - 1.hour, Time.now + 1.hour].sample,
      end_time: [Time.now + 1.hour, Time.now + 45.minute, Time.now + 75.minute].sample,
      location: event.location,
      room: ['114', '022', '201'].sample,
      title: "Slot-#{i}#{j}",
      speaker: ['Peters Christoph', 'Friedrich Chasin', 'Martin Yankov'].sample
    )
    Slot.create!(
      id: 35..40,
      event_id: event.id,
      date: event.start_date + 1.day,
      start_time: [Time.now, Time.now - 1.hour, Time.now + 1.hour].sample,
      end_time: [Time.now + 1.hour, Time.now + 45.minute, Time.now + 75.minute].sample,
      location: event.location,
      room: ['114', '022', '201'].sample,
      title: "Slot-#{i}#{j}#{i}",
      speaker: ['Peters Christoph', 'Friedrich Chasin', 'Martin Yankov'].sample
    )
    Slot.create!(
      id: 45..50,
      event_id: event.id,
      date: event.start_date + 2.day,
      start_time: [Time.now, Time.now - 1.hour, Time.now + 1.hour].sample,
      end_time: [Time.now + 1.hour, Time.now + 45.minute, Time.now + 75.minute].sample,
      location: event.location,
      room: ['114', '022', '201'].sample,
      title: "Slot-#{i}#{j}#{i}#{j}",
      speaker: ['Peters Christoph', 'Friedrich Chasin', 'Martin Yankov'].sample
    )
  end
end